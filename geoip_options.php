<?php
/**
 * GeoIP
 *
 * @plugin     GeoIP
 * @copyright  2016
 * @author     cyp
 * @licence    GNU/GPL
 * @package    SPIP\geoip_options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
