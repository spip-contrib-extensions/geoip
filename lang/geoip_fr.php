<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/monitor/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	// A
	'avertissement' => '* Si vous avez installé la librairie depuis le module apache sur votre serveur, nous vous invitons à vérifier la version installée sur celui-ci.',
	'aide_codeiso' => '0 - Code Iso',
	'aide_codepays' => '1 - Nom du Pays',
	'aide_codeisopays' => '2 - Code Iso du pays',
	'aide_region' => '3 - Région',
	'aide_isoregion' => '4 - Code Iso de la région',
	'aide_ville' => '5 - Ville',
	'aide_codepostal' => '6 - Code postal',
	'aide_latitude' => '7 - Latitude',
	'aide_longitude' => '8 - Longitude',
	'aide_timezone' => '9 - Time Zone',

	// I
	'ip_code_pays' => 'L\'ip @ip@ a le code pays :',
	'ip_code_postal' => 'Le code postal pour cette @ip@ :',
	'ip_latitude_longitude' => 'La latitude et la longitude pour cette @ip@ :',
	'ip_ville' => 'La ville pour cette @ip@ :',

	// L
	'libapache_installe' => 'La librairie Apache est installée, vous n\'avez rien à faire.',

	// T
	'titre_geoip' => 'Test de GeoIP'

);
